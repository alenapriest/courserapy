# Кортежи - неизменяемые списки
empty_typle = ()
empty_typle = tuple()

# создать кортеж с типами
immutables = (int, str, tuple)
print(immutables)

# ошибка при попытке заменить элемент так как кортежи не изменяемые
#immutables[0] = float

# объекты внутри картежей изменяемы
blink = ([], [])
blink[0].append(0)
print(blink)

# используются в качестев ключей в словарях из-за функции hash
print(hash(tuple()))

# при определении кортежа из одного элемента не забывать писать запятую иначе py сочтет переменную типом int
one_element_tuple = (1,)
guess_what = (1)

print(type(guess_what))



