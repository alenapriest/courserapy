#определить списки
# 1 способ - кв скобки
empty_list = []
# 2 способ - литерал list
empty_list = list()

none_list = [None] * 10

collections = ['list', 'tuple', 'dict', 'set']

#список содержащий другие коллекции
user_data = [
    ['Elena', 4.4],
    ['Andrey', 4.2]
]

# длина списка
len(collections)

# обращение к элемантам
print(collections)
print(collections[0])
print(collections[-1])

# доступ по индексу для присваивания
collections[3] = 'frozenset'
print(collections)

# обращение к несуществующему индексу IndexError: list index
# out of range.

# IN - существует ли объект в списке
'tuple' in collections

# СРЕЗЫ
range_list = list(range(10))
print(range_list)

range_list[1:3]
range_list[::2]
range_list[::-1]
range_list[5:1:-1]

# При получении среза создается новый объект
range_list[:] is range_list

# Итерироваться по элементам списка используя цикл FOR
cllections = ['list', 'tuple', 'dict', 'set']
for collections in collections:
    print('Learning {}...'.format(collections))
    # используем функцию format для форматирования строк


# индекс текущего элемента при итерации enumerate
collections = ['list', 'tuple', 'dict', 'set']
for idx, collection in enumerate(collections):
    print('#{} {}'.format(idx, collection))

# добавление элементов
collections.append('OrderedDict')
print(collections)

# расширить список другим списоком extend
collections.extend(['ponyset', 'unicorndict'])
print(collections)

# перегруженный оператор +
collections += [None]
print(collections)

# удаление из списка del
del collections [4]
print(collections)

# мин и макс элемент в массиве, сумма
numbers = [4, 17, 19, 9, 2, 6, 10, 13]
print(min(numbers))
print(max(numbers))
print(sum(numbers))

# преобразовать список в строку str.join()
tag_list = ['python', 'course', 'coursera']
print(', '.join(tag_list))


# Random Случайный список
import random

numbers = []
for _ in range(10): # переменная _
    numbers.append(random.randint(1, 20))

print(numbers)

# сортировка sorted возвращает новый список
print(sorted(numbers))
print(numbers)

# сортировка .sort() in-place (алгоритм TimSort)
numbers.sort()
print(numbers)

# сортировка в обратном порядке
print(sorted(numbers, reverse=True))

numbers.sort(reverse=True)
print(numbers)




