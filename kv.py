import sys
import math
#a = int(sys.argv[1])
#b = int(sys.argv[2])
#c = int(sys.argv[3])

a = 1
b = -3
c = -4

x1 = 0
x2 = 0

d = b ** 2 - 4 * a * c
if d == 0:
    print(-b / 2.0 / a)

elif d > 0:
    x1 = (-int(b) + math.sqrt(d)) / (2 * a)
    x2 = (-int(b) - math.sqrt(d)) / (2 * a)
    print(x1)
    print(x2)
else:
    print("Корней нет")