# СПИСКИ
# Разберём задачу на применение списков --- поиск медианы случайного списка.
# Медиана - это значение в отсортированном списке,
# которое лежит ровно посередине,
# таким образом, половина значений --- слева от него,
# и половина значений --- справа.

import random
# встроенная функция statistics

import statistics


numbers = []
numbers_size = random.randint(10, 15)

for _ in range(numbers_size):
    numbers.append(random.randint(10, 20))
    # randint случайное целое число в переданном ей интервале

print(numbers)

# сортировка
numbers.sort()

half_size = len(numbers) // 2 # половина длины списка
median = None
if numbers_size % 2 == 1:
    median = numbers[half_size]
else:
    median = sum(numbers[half_size - 1:half_size + 1]) / 2

print(median)

# встроенная функция statistics
print(statistics.median(numbers))


